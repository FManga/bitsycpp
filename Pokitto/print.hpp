void inc_txtline();
int32_t minCursorY = 0;
int32_t maxCursorY = POK_LCD_H-8;
int32_t printCount, forceWait;


DigitalIn _aPin(P1_9);
DigitalIn _bPin(P1_4);
DigitalIn _cPin(P1_10);
DigitalIn _upPin(P1_13);
DigitalIn _downPin(P1_3);
DigitalIn _leftPin(P1_25);
DigitalIn _rightPin(P1_7);

void updateButtons(){
  auto old = Buttons::buttons_state;
  int var = 0;
  if (_cPin) var |= (1<<CBIT);
  if (_bPin) var |= (1<<BBIT);
  if (_aPin) var |= (1<<ABIT); // P1_9 = A
  if (_downPin) var |= (1<<DOWNBIT);
  if (_leftPin) var |= (1<<LEFTBIT);
  if (_rightPin) var |= (1<<RIGHTBIT);
  if (_upPin) var |= (1<<UPBIT);

  Buttons::buttons_state = var;
  Buttons::buttons_held = old & var;
}

void wait(){
  do{ updateButtons(); }while( Buttons::buttons_state );
  Core::wait(20);
  do{ updateButtons(); }while( !Buttons::buttons_state );
  do{ updateButtons(); }while( Buttons::buttons_state );
}

void print( const char text[] ){

  auto &cursorX = Display::cursorX;
  auto &cursorY = Display::cursorY;
  const auto &font = Display::font;
  int32_t m_w = POK_LCD_W;

  int end = -1;
    
  for( int i=0; text[i]; ++i ){
    printCount++;

    char c = text[i];
    updateButtons();
    if( !Buttons::buttons_state || forceWait ){
      if( !Buttons::buttons_state ) forceWait = 0;
      Core::wait(30);
    }

    int charstep=0;
  
    if(font[3]) {
      // only caps in this font
      if (c>=97) c-=32;
    }
  
    switch(c) {
    case '\n':			//line feed
      inc_txtline();
      cursorX = 0;
      break;
    case 8:				//backspace
      cursorX -= font[0];
      charstep = Display::print_char(cursorX,cursorY,' ');
      break;
    case 13:			//carriage return
      cursorX = 0;
      break;
    case 14:			//form feed new page(clear screen)
      //clear_screen();
      break;
    default:

      // calculate word width
      int ww = 0;
      if( end == i-1 ){
	
	for( end=i; text[end]>32; end++ ){
	  int ec = text[end];
	  if( font[3] && ec >= 97 ) ec -= 32;
	  ec -= font[2];
	  ww += font[ 4 + ec * (font[0] * ((font[1]>>3)+!!(font[1]&0x7))+1) ] + Display::adjustCharStep;
	}

	ww *= Display::fontSize;

	if( (ww + cursorX) >= m_w && cursorX > 0 ){
	  inc_txtline();
	  cursorX = 0;
	}
	
      }
      
      if (cursorX >= (m_w - font[0])) {
	inc_txtline();
	cursorX = 0;	
      }
      
      if( c<=32 && cursorX == 0 )
	break;
      
      charstep=Display::print_char(cursorX,cursorY,c);
			   
      if (c==' ' && Display::adjustCharStep) charstep=(charstep>>1)+1;
      cursorX += charstep;
    }

  }

}

void fill_txtline(){
  auto top = Display::cursorY;
  auto bottom = Display::cursorY + Display::font[1] + Display::adjustLineStep;
  Pokitto::lcdRectangle( Display::cursorX, top, POK_LCD_W, bottom, COLOR_BLACK );  
}

void inc_txtline(){

  fill_txtline();

  Display::cursorY += Display::font[1] + Display::adjustLineStep;

  if( Display::cursorY >= maxCursorY ){
    wait();
    Pokitto::lcdRectangle( 0, minCursorY, POK_LCD_W, maxCursorY, COLOR_BLACK );  
    Display::cursorY = minCursorY;
  }
  
}
