let fs = require('fs');
let process = require('process');
let child_process = require('child_process');

let inputs = fs.readdirSync('bitsy-archive');

let root = __dirname;

let passCount = 0, failCount = 0

let ignore = {
    'missing.txt':true,
    'index.txt':true,
    'authors.txt':true,
    'README.md':true
};

let file;

process.chdir('Pokitto');

next();

function next(){
    
    if( !inputs.length ){
	console.log(`Finished. ${passCount} passed, ${failCount} failed, ${passCount+failCount} total`);
	return;
    }
    
    file = inputs.pop();

    if( ignore[file] ){
	next();
	return;
    }

    file = root + '/bitsy-archive/' + file;

    if( !fs.statSync(file).isFile() || !/\.txt$/i.test(file) ){
	next();
	return;
    }
    
    child_process.exec('node "' + root + '/index.js" "' + file + '"', (e, so, sr) => {

	if( e ){
	    console.log("FAILED: " + file);
	    failCount++;
	    fs.writeFileSync( file + '.err', sr, 'utf-8' );
	    next();
	}else{
	    compile();
	}

	
    });
    
}

function compile(){

    child_process.exec('make', (e, so, sr) => {
	
	if( e ){
	    console.log("FAILED: " + file);
	    failCount++;
	    fs.writeFileSync( file + '.err', sr, 'utf-8' );
	}else{
	    let outFile = file.replace(/\.txt$/i,'') + '.bin';
	    fs.renameSync('BUILD/HelloWorld.bin', outFile );
	    console.log('PASSED: ' + outFile + ' REMAINING: ' + inputs.length );
	    passCount++;
	}
	
	next();
	
    });
}
